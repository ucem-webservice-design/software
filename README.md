### SE Development Kit 8u111
* [jdk-8u111-windows-x64.exe](https://drive.google.com/open?id=0B0d_E5TqKc7NY19ZZkllUF9JYUU)

### Apache Maven
 * [apache-maven-3.3.9-bin.zip](https://drive.google.com/open?id=0B0d_E5TqKc7NVVVpNVFrazV2WVE)
 * [Como instalar Maven en Windows](https://www.mkyong.com/maven/how-to-install-maven-in-windows/)

### Interfaz gráfica para la creación de peticiones HTTP.
* [postman](https://www.getpostman.com/)
* [insomnia](https://insomnia.rest/)

### Editor para el diseño y documentación de interfaces RESTful.
* [Swagger editor](http://swagger.io/swagger-editor/)  

### Herramienta de prueba de carga para analizar y medir el desempeño.
* [apache-jmeter-3.1.tgz](http://www-eu.apache.org/dist//jmeter/binaries/apache-jmeter-3.1.tgz)
* [apache-jmeter-3.1.zip](http://www-eu.apache.org/dist//jmeter/binaries/apache-jmeter-3.1.zip)
